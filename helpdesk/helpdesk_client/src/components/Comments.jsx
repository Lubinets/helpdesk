import React, { Component } from 'react';
import { SERVER_URL } from '../constants';
import axios from 'axios';
import DataTable from './DataTable';

class Comments extends DataTable {
    componentDidMount() {
        axios.get(SERVER_URL + "/comments/tickets/" + this.props.id)
        .then(response => {
            this.setState({data: response.data});
        }).catch(error => {
            
        });
    }
}

export default Comments;