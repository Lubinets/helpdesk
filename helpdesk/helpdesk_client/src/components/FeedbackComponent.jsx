import React, { Component } from 'react';
import axios from 'axios';
import {SERVER_URL} from '../constants';
import StarRatingComponent from 'react-star-rating-component';
import HeaderComponent from './HeaderComponent';
import AuthenticationService from '../services/AuthenticationService';
import '../css/FeedbackComponent.css'

class FeedbackComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: this.props.location.state.edit,
            rate: null,
            text: '',
            feedback: {}
        };

        this.onChangeRating = this.onChangeRating.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onClickSubmit = this.onClickSubmit.bind(this);
        this.onClickBack = this.onClickBack.bind(this);
    }

    loadFeedback() {
        axios.get(SERVER_URL + '/feedback?ticket=' + this.props.match.params.id)
        .then(response => {
            this.setState({
                feedback: response.data
            })
        }).catch(error => {
            alert(error.response.date);
        })
    }

    componentDidMount() {
        AuthenticationService.setupAxiosInterceptors();
        if(this.state.edit === false) {
            this.loadFeedback();
        }
    }

    onClickBack() {
        window.history.back();
    }

    onClickSubmit() {
        let data = {
            rate: this.state.rate,
            text: this.state.text
        };
        axios.post(SERVER_URL + '/feedback?ticket=' + this.props.match.params.id, data)
        .then(response => {
            alert(response.statusText);
            this.onClickBack();
        }).catch(error => {
            alert(error.response.data)
        });
    }

    onChangeRating(nextValue, prevValue, name) {
        this.setState({
            rate: nextValue
        })
    }

    onChangeText(e) {
        this.setState({
            text: e.target.value
        }) 
    }

    render() {
        let render = this.state.edit
            ?<div className="center">
                <h2>Please, rate your satisfaction with the solution</h2>
                <StarRatingComponent
                    name="Rate"
                    onStarClick={this.onChangeRating}
                    editing={this.state.edit}>
                </StarRatingComponent>
                <textarea className="feedback-textarea"  onChange={this.onChangeText}></textarea>
                <div className='right'>
                    <button onClick={this.onClickSubmit}>Submit</button>
                </div>
            </div>
            :<div className="center">
                <h2>Feedback</h2>
                <StarRatingComponent
                    name="Rate"
                    value={this.state.feedback.rate}
                    editing={this.state.edit}>
                </StarRatingComponent>
                <textarea className="feedback-textarea" value={this.state.feedback.text} readOnly></textarea>
            </div>
        return(
            <div className="left">
                <HeaderComponent/>
                <button onClick={this.onClickBack}>Back</button>
                {render}
            </div>
        )
    }
}

export default FeedbackComponent;