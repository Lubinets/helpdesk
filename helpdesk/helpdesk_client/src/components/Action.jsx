import React, { Component } from 'react';
import ActionService from '../services/ActionService';
import '../css/Action.css';

class Action extends Component {
    constructor(props) {
        super(props);
        this.state = {
            actionList: []
        }
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        ActionService.getActionList(this.props.state)
            .then(response => {
                this.setState({
                    actionList: response.data
                })
            }).catch(error => {
                console.log(error.response.data)
            });
    }

    onChange(e) {
        if(e.target.value !== '-1') {
            ActionService.changeState(this.props.ticketId, e.target.value)
            .then(
                window.location.reload()
            ).catch(error =>
                alert(error.response.data)
            );
        } else {
            this.props.history.push({
                pathname :'/feedback/' + this.props.ticketId,
                state : {edit: false}
            });
        }
    }

    render() {
        return (
            <select onChange={this.onChange} className='action-select'>
                <option>Select Action</option>
                {
                    this.state.actionList.map(action =>
                        <option key={action.id} value={action.newState}>{action.name}</option>
                    )
                }
                {this.props.state === "DONE" ? <option value={-1}>Feedback</option> : ''}
            </select>
        )
    }
}

export default Action;