import React, { Component } from 'react';
import axios from 'axios';
import { SERVER_URL} from '../constants';
import AuthenticationService from '../services/AuthenticationService';

class DownloadComponent extends Component {

    componentDidMount() {
        AuthenticationService.setupAxiosInterceptors();
        axios({
            url : SERVER_URL + "/attachments/" + this.props.match.params.id,
            responseType: 'blob',
        }).then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            const filename = response.request.getResponseHeader('Content-Disposition').split('filename=')[1]
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
        })
    }

    render() {
        return(<>Downloading...</>);
    }
}

export default DownloadComponent;