import React, { Component } from 'react';
import Utility from '../Utility';

class DataTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    render() {
        let keys = [];
        this.state.data.map(item => {
                keys = Object.keys(item);
            }
        );
        return(
            <table className="table">
                <tbody>
                    <tr>
                        {keys.map(key => 
                            <td key={key}>
                                {Utility.transformKeyText(key)}
                            </td>
                        )}
                    </tr>
                    {this.state.data.map(item => 
                        <tr>
                            {keys.map(key =>
                                <td key={key.id}>{item[key]}</td>
                            )}
                        </tr>
                    )}
                </tbody>
            </table>
        )
    }
}

export default DataTable;