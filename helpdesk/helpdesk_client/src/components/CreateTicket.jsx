import React, { Component } from 'react';
import '../css/CreateTicket.css';
import axios from 'axios';
import { SERVER_URL} from '../constants';

class CreateTicket extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            urgencyList: [],
            formFields: {},
            file: null,
            header: "Create New Ticket"
        };
        this.onSubmitDraft = this.onSubmitDraft.bind(this);
        this.onSubmitNew = this.onSubmitNew.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFileChange = this.onFileChange.bind(this);
    }

    componentDidMount() {
        axios.get(SERVER_URL + '/urgencies')
        .then(response => {
            this.setState({urgencyList: response.data});
        }).catch(error => {
            console.log("Can't get data from server")
        });
        axios.get(SERVER_URL + '/categories')
        .then(response => {
            this.setState({categories: response.data});
        }).catch(error => {
            console.log("Can't get data from server")
        });
    }

    onInputChange(e) {
        let formFields = {...this.state.formFields};
        formFields[e.target.name] = e.target.value;
        this.setState({
            formFields
        });
    }

    onFileChange(e) {
        this.setState({
            file: e.target.files[0]
        })
    }

    onSubmitDraft(e) {
        e.preventDefault();
        const formData = new FormData();
        const ticket = new Blob([JSON.stringify(this.state.formFields)], {type: "application/json"});
        formData.append('ticket', ticket);
        formData.append("file", this.state.file);
        const headers = {'Content-Type': undefined};

        axios.post(SERVER_URL + '/tickets?draft=true', formData, headers)
        .then(response => {
            alert(response.statusText);
            this.props.closeFunction();
            this.props.onTicketCreated();
        }).catch(error => {
            alert(error.response.data)
        });
    }

    onSubmitNew(e) {
        e.preventDefault();
        const formData = new FormData();
        const ticket = new Blob([JSON.stringify(this.state.formFields)], {type: "application/json"});
        formData.append('ticket', ticket);
        formData.append('file', this.state.file);
        const headers = {'Content-Type': undefined};
        axios.post(SERVER_URL + '/tickets?draft=false', formData, headers)
        .then(response => {
            alert(response.statusText);
            this.props.closeFunction();
            this.props.onTicketCreated();
        }).catch(error => {
            alert(error.response.data)
        });
    }

    render() {
        const now = new Date().toISOString().split("T")[0];
        return(
            <form id='create-form' method='POST'>
                <button onClick={this.props.closeFunction} id="close-button"></button>
                <h2>{this.state.header}</h2>
                <table>
                    <tbody>
                        <tr>             
                            <td><label>Category<span className='red'> *</span></label></td>
                            <td>
                                <select onChange={this.onInputChange} name='category'>
                                    <option value={null}>Select category</option>
                                    {
                                        this.state.categories.map(item => 
                                            <option key={item.id} value={item.id}>{item.name}</option>
                                        )
                                    }
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Name<span className='red'> *</span></label></td>
                            <td><input onChange={this.onInputChange} name='name' type='text'></input></td>
                        </tr>
                        <tr>
                            <td><label>Description:</label></td>
                            <td><textarea onChange={this.onInputChange} name='description'></textarea></td>
                        </tr>
                        <tr>
                            <td><label>Urgency<span className='red'> *</span></label></td>
                            <td>
                                <select onChange={this.onInputChange} name='urgency'>
                                    <option value={null}>Select urgency</option>
                                    {
                                        this.state.urgencyList.map(item => 
                                            <option key={item} value={item}>{item}</option>
                                        )
                                    }
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Desired resolution date:</label></td>
                            <td>
                                <input type='date' onChange={this.onInputChange} name='desiredResolutionDate' min={now}/>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Add attachments:</label></td>
                            <td>
                                <input onChange={this.onFileChange} 
                                    name='attachment' 
                                    type='file' 
                                    accept='.jpeg,.png,.jpeg,.pdf,.doc,.docx'>
                                </input>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Comment:</label></td>
                            <td><textarea onChange={this.onInputChange} name='comment'></textarea></td>
                        </tr>
                        <tr>
                            <td><input type='submit' onClick={this.onSubmitDraft} value='Save as Draft'/></td>
                            <td><input type='submit' onClick={this.onSubmitNew} value='Submit'/></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        )
    }
}

export default CreateTicket;