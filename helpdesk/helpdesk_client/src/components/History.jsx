import DataTable from './DataTable';
import { SERVER_URL } from '../constants';
import axios from 'axios';
import AuthenticationService from '../services/AuthenticationService';

class History extends DataTable {
    componentDidMount() {
        AuthenticationService.setupAxiosInterceptors();
        axios.get(SERVER_URL + "/history/tickets/" + this.props.id)
        .then(response => {
            this.setState({data: response.data});
        }).catch(error => {
            console.log("Can't load data from the server")
        });
    }
}

export default History;