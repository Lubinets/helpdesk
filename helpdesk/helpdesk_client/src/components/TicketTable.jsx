import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Utility from '../Utility';
import Action from './Action';

class TicketTable extends Component {
    render() {
        let keys = [];
        this.props.data.map(item => 
            keys = Object.keys(item)           
        );
                   
        return(
            <table className="table">
                <tbody>
                    <tr>
                        {keys.map(key => 
                            <td onClick={this.props.onClickFilter} key={key} id={key}>
                                {Utility.transformKeyText(key)}
                            </td>
                        )}
                        <td>Action</td>
                    </tr>
                    {this.props.data.map(item => 
                        <tr>
                            {keys.map(key =>
                                key === "name" 
                                ? <td key={key.id}><Link to={"/ticket-overview/"+item.id}>{item[key]}</Link></td>
                                : <td key={key.id}>{item[key]}</td>

                            )}
                            <td>
                                <Action history={this.props.history} state={item.state} ticketId={item.id}/>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        )
    }
}

export default TicketTable;