import React, { Component } from 'react';
import '../css/TicketForm.css';
import TicketTable from './TicketTable';
import HeaderComponent from './HeaderComponent';
import axios from 'axios';
import {SERVER_URL} from '../constants';
import AuthenticationService from '../services/AuthenticationService';
import CreateTicket from './CreateTicket';

class TicketForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tickets: [],
            url: this.getUrl(),
            params: {},
            filter: 'id',
            sort: 'asc',
            valid: true,
            createForm: false,
        }
        this.onChange = this.onChange.bind(this);
        this.onClickAllTickets = this.onClickAllTickets.bind(this);
        this.onClickMyTickets = this.onClickMyTickets.bind(this);
        this.openCreateWindow = this.openCreateWindow.bind(this);

        this.myTickets = React.createRef();
        this.allTickets = React.createRef();
    }

    componentDidMount() {
        AuthenticationService.setupAxiosInterceptors();
        this.loadData(this.state.url, this.state.params);
    }



    loadData(url, params) {
        axios.get(url, params)
        .then(response => {
            this.setState({tickets: response.data});
        }).catch(error => {
            if(error.response === undefined) {
                console.log("Cannot load data from the server");
            } else if(error.response.status === 404) {
                console.log("No tickets found for the user");
            }
        })
    }

    onChange(event) {
        if(event.target.value === "") {
            this.loadData(this.state.url, this.state.params);
            this.setState({valid: true});
        } else {
            if(this.validateFilter(event.target.value)) {
                this.setState({valid: true});
                const url = this.state.url + "&" + this.state.filter + "=" + event.target.value;
                this.loadData(url, this.state.params);
            } else {
                this.setState({valid: false});
            }
        }
    }

    onClickAllTickets() {
        this.allTickets.current.classList.add('active');
        this.myTickets.current.classList.remove('active');

        this.loadData(this.getUrl(), this.state.params);
        this.setState(
            {
                url: this.getUrl(),
            }
        );
    }

    getUrl() {
        let user = AuthenticationService.getUser();
        if(user.role === "ROLE_EMPLOYEE") {
            return SERVER_URL + "/tickets/employees/" + user.id + "?";
        } else if(user.role === "ROLE_MANAGER") {
            return SERVER_URL + "/tickets/managers/" + user.id + "?"; 
        } else if(user.role === "ROLE_ENGINEER") {
            return SERVER_URL + "/tickets/engineers/" + user.id + "?";
        }
    }

    onClickMyTickets() {
        this.myTickets.current.classList.add('active');
        this.allTickets.current.classList.remove('active');

        this.loadData(this.state.url + "tickets=my", this.state.params);
        this.setState((prevState) => ({
            url : prevState.url + "tickets=my"
        }));
    }

    onClickFilter = (event) => {
        document.getElementById(this.state.filter).classList.remove('active');
        event.target.classList.add('active');
        if(this.state.sort === 'desc') {
            this.loadData(this.state.url, {params: {sort : event.target.id + ',asc'}});
            this.setState({
                params : {params: {sort : event.target.id + ',asc'}},
                filter : event.target.id,
                sort: 'asc'
            });
        } else {
            this.loadData(this.state.url, {params: {sort : event.target.id + ',desc'}});
            this.setState({
                params : {params: {sort : event.target.id + ',desc'}},
                filter : event.target.id,
                sort: 'desc'
            });
        }
    }

    validateFilter(filterText) {
        const regex = /^[a-zA-Z0-9~."(),:;<>;@\[\]!#$%&*+-\/=?^_{|}]+$/g;
        return regex.test(filterText);
    }

    openCreateWindow() {
        this.setState({
            createForm: true
        })
    }

    closeCreateWindow = () => {
        this.setState({
            createForm: false
        })
    }

    onDataChanged = () => {
        this.loadData(this.state.url, this.state.params)
    }

    render() {
        return(
            <>
                <HeaderComponent/>
                {this.state.createForm ? <CreateTicket onTicketCreated={this.onDataChanged} closeFunction={this.closeCreateWindow}/> : ''}
                { 
                    AuthenticationService.getUser().role !== 'ROLE_ENGINEER'
                    ? <div id='create-btn-block'>
                        <button onClick={this.openCreateWindow}>Create New Ticket</button>
                    </div>
                    : ''
                }
                <div className='btn-block'>
                    <button className='active' ref={this.allTickets} onClick={this.onClickAllTickets}>All Tickets</button>
                    <button ref={this.myTickets} onClick={this.onClickMyTickets}>My Tickets</button>
                </div>
                {this.state.valid ? '' : <div className="error">
                    It is allowed to enter only upper and lowercase English alpha characters, digits and special characters:
                </div>}
                <input  onChange={this.onChange} id='filter' type='text'/>
                <TicketTable onActionPerformed={this.onDataChanged} history={this.props.history}
                    onClickFilter={this.onClickFilter} data={this.state.tickets}/>
            </>
        )
    }
}

export default TicketForm;