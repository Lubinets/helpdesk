import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './css/App.css';
import LoginComponent from './components/LoginComponent';
import AuthenticatedRoute from './components/AuthenticatedRoute';
import TicketForm from './components/TicketForm';
import TicketOverview from './components/TicketOverview';
import DownloadComponent from './components/DownloadComponent';
import FeedbackComponent from './components/FeedbackComponent';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path='/' exact component={LoginComponent}/>
          <Route path='/login' exact component={LoginComponent}/>
          <AuthenticatedRoute path="/all-tickets" exact component={TicketForm}/>
          <AuthenticatedRoute path="/ticket-overview/:id" component={TicketOverview}/>
          <AuthenticatedRoute path="/download/:id" component={DownloadComponent}/>
          <AuthenticatedRoute path="/feedback/:id" component={FeedbackComponent}/>
        </Switch>
      </Router>

    </div>
  );
}

export default App;
