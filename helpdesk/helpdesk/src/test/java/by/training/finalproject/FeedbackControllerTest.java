package by.training.finalproject;

import by.training.finalproject.config.RootConfig;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.dto.FeedbackDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class FeedbackControllerTest {
    private MockMvc mvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;

    
    @Before
    public void setup() throws NotFoundException {
        mvc = MockMvcBuilders
        .webAppContextSetup(webApplicationContext)
        .build();
    }
    
    @Test
    public void getFeedbackByTicketIdTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
        .get("/feedback")
        .param("ticket", "4")
        .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(MockMvcResultMatchers.jsonPath("$.rate").exists());
    }
    
    @Test
    public void getFeedbackByTicketIdNegativeTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
        .get("/feedback")
        .param("ticket", "5")
        .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isNotFound());
    }
    
    @Test
    public void addFeedbackTest() throws Exception {
        User user = new User("user1_mogilev@yopmail.com", "", AuthorityUtils.createAuthorityList("ROLE_EMPLOYEE"));
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        
        mvc.perform(MockMvcRequestBuilders
        .post("/feedback")
        .principal(testingAuthenticationToken)
        .param("ticket", "4")
        .content(asJsonString(new FeedbackDTO((short) 4, "test")))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
    }
    
    @Test
    public void addFeedbackNegativeTest() throws Exception {
        User user = new User("user1_mogilev@yopmail.com", "", AuthorityUtils.createAuthorityList("ROLE_EMPLOYEE"));
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(user, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
        
        mvc.perform(MockMvcRequestBuilders
        .post("/feedback")
        .principal(testingAuthenticationToken)
        .param("ticket", "2")
        .content(asJsonString(new FeedbackDTO((short) 4, "test")))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isForbidden());
    }
    
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
