CREATE TABLE IF NOT EXISTS Category
(
    id BIGINT not null,
    name VARCHAR,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS User
(
    id BIGINT not null,
    first_name VARCHAR,
    last_name VARCHAR,
    role_id ENUM ('ROLE_EMPLOYEE', 'ROLE_MANAGER', 'ROLE_ENGINEER'),
    email VARCHAR,
    password VARCHAR,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Ticket
(
    id BIGINT not null AUTO_INCREMENT,
    name VARCHAR,
    description VARCHAR,
    created_on DATE,
    desired_resolution_date DATE,
    assignee_id BIGINT,
    owner_id BIGINT,
    state_id INTEGER,
    category_id BIGINT,
    urgency_id INTEGER,
    approver_id BIGINT,
    PRIMARY KEY(id),
    FOREIGN KEY(assignee_id) REFERENCES User(id),
    FOREIGN KEY(owner_id) REFERENCES User(id),
    FOREIGN KEY(category_id) REFERENCES Category(id),
    FOREIGN KEY(approver_id) REFERENCES User(id)
);

CREATE TABLE IF NOT EXISTS Attachment
(
    id BIGINT not null AUTO_INCREMENT,
    blob BLOB,
    ticket_id BIGINT,
    name VARCHAR,
    content_type VARCHAR,
    PRIMARY KEY(id),
    FOREIGN KEY(ticket_id) REFERENCES Ticket(id)
);

CREATE TABLE IF NOT EXISTS History
(   
    id BIGINT not null AUTO_INCREMENT,
    ticket_id BIGINT,
    date TIMESTAMP,
    action VARCHAR,
    user_id BIGINT,
    description VARCHAR,
    PRIMARY KEY(id),
    FOREIGN KEY(ticket_id) REFERENCES Ticket(id),
    FOREIGN KEY(user_id) REFERENCES User(id)
);

CREATE TABLE IF NOT EXISTS Comment
(
    id BIGINT not null AUTO_INCREMENT,
    user_id BIGINT,
    text VARCHAR,
    creation_date DATE,
    ticket_id BIGINT,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id) REFERENCES User(id),
    FOREIGN KEY(ticket_id) REFERENCES Ticket(id)
);

CREATE TABLE IF NOT EXISTS Feedback
(
    id BIGINT not null AUTO_INCREMENT,
    user_id BIGINT,
    rate SMALLINT,
    date DATE,
    text VARCHAR,
    ticket_id BIGINT,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id) REFERENCES User(id),
    FOREIGN KEY(ticket_id) REFERENCES Ticket(id)
);

CREATE TABLE IF NOT EXISTS Action
(
    id BIGINT not null,
    name VARCHAR,
    role_id ENUM ('ROLE_EMPLOYEE', 'ROLE_MANAGER', 'ROLE_ENGINEER'),
    old_state INTEGER,
    new_state INTEGER,
    mail_template VARCHAR,
    PRIMARY KEY(id)
);


MERGE INTO User (id, first_name, last_name, role_id, email, password) VALUES (1, 'USER_1', 'USER_1', 'ROLE_EMPLOYEE', 'user1_mogilev@yopmail.com', '$2a$10$WgqLwpdj.e2Sjy1ccLBz1uXMM73DF6mHP2Vt3XMW2ngvvDoX2d22y');
MERGE INTO User (id, first_name, last_name, role_id, email, password) VALUES (2, 'MANAGER_1', 'MANAGER_1', 'ROLE_MANAGER', 'manager1_mogilev@yopmail.com', '$2a$10$WgqLwpdj.e2Sjy1ccLBz1uXMM73DF6mHP2Vt3XMW2ngvvDoX2d22y');
MERGE INTO User (id, first_name, last_name, role_id, email, password) VALUES (3, 'ENGINEER_1', 'ENGINEER_1', 'ROLE_ENGINEER', 'engineer1_mogilev@yopmail.com', '$2a$10$WgqLwpdj.e2Sjy1ccLBz1uXMM73DF6mHP2Vt3XMW2ngvvDoX2d22y');
MERGE INTO User (id, first_name, last_name, role_id, email, password) VALUES (4, 'USER_2', 'USER_2', 'ROLE_EMPLOYEE', 'user2_mogilev@yopmail.com', '$2a$10$WgqLwpdj.e2Sjy1ccLBz1uXMM73DF6mHP2Vt3XMW2ngvvDoX2d22y');
MERGE INTO User (id, first_name, last_name, role_id, email, password) VALUES (5, 'MANAGER_2', 'MANAGER_2', 'ROLE_MANAGER', 'manager2_mogilev@yopmail.com', '$2a$10$WgqLwpdj.e2Sjy1ccLBz1uXMM73DF6mHP2Vt3XMW2ngvvDoX2d22y');
MERGE INTO User (id, first_name, last_name, role_id, email, password) VALUES (6, 'ENGINEER_2', 'ENGINEER_2', 'ROLE_ENGINEER', 'engineer2_mogilev@yopmail.com', '$2a$10$WgqLwpdj.e2Sjy1ccLBz1uXMM73DF6mHP2Vt3XMW2ngvvDoX2d22y');

MERGE INTO Category VALUES (1, 'Application & Services');
MERGE INTO Category VALUES (2, 'Benefits & Paper');
MERGE INTO Category VALUES (3, 'Hardware & Software');
MERGE INTO Category VALUES (4, 'People Management');
MERGE INTO Category VALUES (5, 'Security & Access');
MERGE INTO Category VALUES (6, 'Workplaces & Facilities');

MERGE INTO Ticket VALUES (1, 'TEST1', 'TEST1', TO_DATE('17-06-2019', 'DD-MM-yyyy'), TO_DATE('21-06-2019', 'DD-MM-yyyy'), 3, 2, 5, 1, 3, 5);
MERGE INTO Ticket VALUES (2, 'TEST2', 'TEST2', TO_DATE('18-06-2019', 'DD-MM-yyyy'), TO_DATE('22-06-2019', 'DD-MM-yyyy'), null, 1, 2, 1, 2, 2);
MERGE INTO Ticket VALUES (3, 'TEST3', 'TEST3', TO_DATE('20-06-2019', 'DD-MM-yyyy'), TO_DATE('23-06-2019', 'DD-MM-yyyy'), null, 4, 1, 1, 1, null);
MERGE INTO Ticket VALUES (4, 'TEST4', 'TEST4', TO_DATE('20-06-2019', 'DD-MM-yyyy'), TO_DATE('23-06-2019', 'DD-MM-yyyy'), 3, 1, 6, 1, 1, 2);

MERGE INTO Feedback VALUES (1, 1, 3, TO_DATE('17-06-2019', 'DD-MM-yyyy'), 'test feedback', 4);

MERGE INTO Comment VALUES (1, 1, 'Test comment 1', TO_DATE('2019-06-17', 'yyyy-MM-DD'), 1);
MERGE INTO Comment VALUES (2, 1, 'Test comment 2', TO_DATE('2019-06-17', 'yyyy-MM-DD'), 1);

MERGE INTO Action VALUES (1, 'Submit', 'ROLE_EMPLOYEE', 0, 1, 'TicketCreatedTemplate');
MERGE INTO Action VALUES (2, 'Cancel', 'ROLE_EMPLOYEE', 0, 4, null);
MERGE INTO Action VALUES (3, 'Submit', 'ROLE_MANAGER', 0, 1, 'TicketCreatedTemplate');
MERGE INTO Action VALUES (4, 'Cancel', 'ROLE_MANAGER', 0, 4, null);
MERGE INTO Action VALUES (5, 'Approve', 'ROLE_MANAGER', 1, 2, 'TicketApprovedTemplate');
MERGE INTO Action VALUES (6, 'Decline', 'ROLE_MANAGER', 1, 3, 'TicketDeclinedTemplate');
MERGE INTO Action VALUES (7, 'Cancel', 'ROLE_MANAGER', 1, 4, 'TicketCancelledManagerTemplate');
MERGE INTO Action VALUES (8, 'Assign to Me', 'ROLE_ENGINEER', 2, 5, null);
MERGE INTO Action VALUES (9, 'Cancel', 'ROLE_ENGINEER', 2, 4, 'TicketCancelledEnginnerTemplate');
MERGE INTO Action VALUES (10, 'Submit', 'ROLE_EMPLOYEE', 3, 1, 'TicketCreatedTemplate');
MERGE INTO Action VALUES (11, 'Cancel', 'ROLE_EMPLOYEE', 3, 4, null);
MERGE INTO Action VALUES (12, 'Submit', 'ROLE_MANAGER', 3, 1, null);
MERGE INTO Action VALUES (13, 'Cancel', 'ROLE_MANAGER', 3, 4, null);
MERGE INTO Action VALUES (14, 'Done', 'ROLE_ENGINEER', 5, 6, 'TicketDoneTemplate');


