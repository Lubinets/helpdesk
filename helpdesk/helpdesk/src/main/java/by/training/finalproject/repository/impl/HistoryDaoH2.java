package by.training.finalproject.repository.impl;

import by.training.finalproject.model.History;
import by.training.finalproject.repository.*;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HistoryDaoH2 implements HistoryDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<History> getHistoryList(long ticketId) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from History where ticket.id = :id "
                        + "order by date desc", History.class);
        query.setMaxResults(5);
        query.setParameter("id", ticketId);
        return query.list();
    }
    
    @Override
    public void save(History historyRecord) {
        sessionFactory.getCurrentSession()
                .persist(historyRecord);
    } 
}
