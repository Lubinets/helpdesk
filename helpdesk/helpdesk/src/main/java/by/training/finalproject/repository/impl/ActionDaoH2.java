package by.training.finalproject.repository.impl;

import by.training.finalproject.model.Role;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Action;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.User;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import by.training.finalproject.repository.ActionDao;
import java.util.Optional;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

@Repository
public class ActionDaoH2 implements ActionDao {
    private final Logger logger = LogManager.getLogger(this.getClass());
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Action> getAllAllowed(State state, Role role) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Action "
                        + "where oldState = :oldState and "
                        + "role  = :role", Action.class);
        query.setParameter("oldState", state);
        query.setParameter("role", role);
        return query.list();
    }

    @Override
    public Optional isExist(Ticket ticket, State newState, User user) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Action "
                        + "where oldState = :oldState and "
                        + "newState = :newState and "
                        + "role = :role", Action.class);
        State oldState = ticket.getState();
        query.setParameter("oldState", oldState);
        query.setParameter("newState", newState);
        query.setParameter("role", user.getRole());
        
        if(oldState == State.DRAFT || oldState == State.DECLINED) {
            if(!ticket.getTicketOwner().equals(user)) {
                return Optional.empty();
            }
        } else if(oldState == State.NEW) {
            if(!ticket.getTicketOwner().getRole().equals(Role.ROLE_EMPLOYEE)) {
                return Optional.empty();
            }
        }
        return Optional.ofNullable(query.list().get(0)); 
    }

}
