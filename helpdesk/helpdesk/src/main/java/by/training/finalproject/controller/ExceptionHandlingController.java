package by.training.finalproject.controller;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import java.io.IOException;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingController {
    private final Logger logger = LogManager.getLogger(this.getClass());
    
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleConstraintViolation(Exception ex) {
        Set<ConstraintViolation<?>> violations = ((ConstraintViolationException) ex).getConstraintViolations();
        StringBuilder builder = new StringBuilder();
        for (ConstraintViolation<?> violation : violations) {
            builder.append(violation.getMessage());
            builder.append("\n");
        }
        return ResponseEntity.badRequest().body(builder.toString());
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<?> handleIOException(Exception ex) {
        logger.error(ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(Exception ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(NoPermissionException.class)
    public ResponseEntity<?> handleNoPermissionException(Exception ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(ex.getMessage());
    }
    
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleIllegalArgumentException(Exception ex) {
        return ResponseEntity.badRequest().build();
    }
    
    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleOtherExceptions(Exception ex) {
        logger.error(ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
