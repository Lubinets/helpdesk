package by.training.finalproject.model.dto;

import by.training.finalproject.model.State;
import by.training.finalproject.model.Urgency;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.Data;

@Data
public class TicketDTO {
    private long id;
    
    private String name;
    
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date desiredResolutionTime;
    
    private Urgency urgency;
    
    private State state;
}
