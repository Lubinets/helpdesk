package by.training.finalproject.repository.impl;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Attachment;
import by.training.finalproject.repository.*;
import java.util.Optional;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AttachmentDaoH2 implements AttachmentDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Optional<Attachment> getById(long id) {
        return Optional.ofNullable(
                sessionFactory.getCurrentSession()
                .get(Attachment.class, id));
    }

    @Override
    public void removeAttachment(long id) throws NotFoundException {
        Optional<Attachment> attachment = getById(id);
        if(!attachment.isPresent()) {
            throw new NotFoundException();
        }
        sessionFactory.getCurrentSession().delete(attachment.get());
    }

}
