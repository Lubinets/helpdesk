package by.training.finalproject.controller;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Feedback;
import by.training.finalproject.model.dto.FeedbackDTO;
import by.training.finalproject.service.FeedbackService;
import java.security.Principal;
import javax.mail.MessagingException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(path = "/feedback", produces = MediaType.APPLICATION_JSON_VALUE)
public class FeedbackController {
    private final FeedbackService service;
    
    private final ModelMapper mapper;
    
    @Autowired
    public FeedbackController(FeedbackService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }
    
    @GetMapping
    public ResponseEntity<?> getFeedbackByTicketId(
            @RequestParam(value = "ticket") long ticketId)
            throws NotFoundException {
        return ResponseEntity.ok(mapper.map(service.getByTicketId(ticketId), FeedbackDTO.class));
    }
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addFeedback(
            @RequestBody FeedbackDTO feedback,
            @RequestParam(value = "ticket") long ticketId,
            Principal principal,
            UriComponentsBuilder builder)
            throws NotFoundException, NoPermissionException, MessagingException {
        UriComponents uriComponents
                = builder.path("/feedback").buildAndExpand();
        service.createFeedback(ticketId, principal, mapper.map(feedback, Feedback.class));
        return ResponseEntity.created(uriComponents.toUri()).body(feedback);
    }
}
