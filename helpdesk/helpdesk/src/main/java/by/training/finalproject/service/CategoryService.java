package by.training.finalproject.service;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Category;
import by.training.finalproject.repository.CategoryDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryService {
    @Autowired
    private CategoryDao categoryDao;
    
    @Transactional
    public List<Category> getAll() throws NotFoundException {
        List<Category> categories = categoryDao.getAll();
        if(!categories.isEmpty()) {
            return categories;
        } else {
            throw new NotFoundException();
        }
    }
}
