package by.training.finalproject.repository;

import by.training.finalproject.model.Role;
import by.training.finalproject.model.User;
import java.util.List;
import java.util.Optional;


public interface UserDao {
    /**
     * Returns user by id.
     * @param id id of the user
     * @return Optional of user
     */
    public Optional<User> get(long id);
    
    /**
     * Returs user by his login/email.
     * @param name email of the user
     * @return Optional of user
     */
    public Optional<User> getByName(String name);
    
    /**
     * Returns all users with chosen role
     * @param role role of the users
     * @return list of users
     */
    public List<User> getByRole(Role role);
}
