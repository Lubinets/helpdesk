package by.training.finalproject.repository;

import by.training.finalproject.model.Role;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Action;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.User;
import java.util.List;
import java.util.Optional;


public interface ActionDao {
    /**
     * Gets all allowed actions for specific new state and user role.
     * @param state new state of the ticket
     * @param role user role
     * @return returns list of allowed actions
     */
    public List<Action> getAllAllowed(State state, Role role);
    
    /**
     * Check if there is a valid action with set parameters for ticket.
     * @param ticket ticket to check if action is allowed
     * @param newState new state of ticket to set
     * @param user user which initiated state change
     * @return Optional of action
     */
    public Optional<Action> isExist(Ticket ticket, State newState, User user);
}
