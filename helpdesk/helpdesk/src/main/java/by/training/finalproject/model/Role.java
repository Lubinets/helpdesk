package by.training.finalproject.model;


public enum Role {
    ROLE_EMPLOYEE(1),
    ROLE_MANAGER(2),
    ROLE_ENGINEER(3);

    private final int id;

    Role(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}