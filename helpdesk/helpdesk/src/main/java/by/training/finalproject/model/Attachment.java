package by.training.finalproject.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Attachment")
@Data
public class Attachment implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "blob")
    private byte[] blob;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "content_type")
    private String contentType;
    
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;
}
