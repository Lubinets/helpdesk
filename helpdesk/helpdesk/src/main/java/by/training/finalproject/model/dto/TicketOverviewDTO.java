package by.training.finalproject.model.dto;

import by.training.finalproject.model.Attachment;
import by.training.finalproject.model.Category;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Urgency;
import by.training.finalproject.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.Data;

@Data
public class TicketOverviewDTO {
    private long id;
    
    private String name;
    
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date createdOn;
    
    private State state;
    
    private String category;
    
    private Urgency urgency;
    
    private String description;
        
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date desiredResolutionDate;
    
    private String ticketOwner;
    
    private String approver;
    
    private String ticketAssignee;
    
    private List<Long> attachments;
    
    /**
     * @param category the Category to set
     */
    public void setCategory(Category category) {
        this.category = category.getName();
    }
    
    /**
     * @param ticketOwner the ticketOwner to set
     */
    public void setTicketOwner(User ticketOwner) {
        this.ticketOwner = ticketOwner.getFirstName() + " " + ticketOwner.getLastName();
    }

    /**
     * @param approver the approver to set
     */
    public void setApprover(User approver) {
        this.approver = approver.getFirstName() + " " + approver.getLastName();
    }

    /**
     * @param ticketAssignee the ticketAssignee to set
     */
    public void setTicketAssignee(User ticketAssignee) {
        this.ticketAssignee = ticketAssignee.getFirstName() + " " + ticketAssignee.getLastName();
    }

    /**
     * @param attachments the attachments to set
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = new ArrayList();
        for(Attachment attachment : attachments) {
            this.attachments.add(attachment.getId());
        }
    }
}
