package by.training.finalproject.controller;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.User;
import by.training.finalproject.model.dto.UserDTO;
import by.training.finalproject.service.UserService;
import java.security.Principal;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {

    @Autowired
    private UserService service;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/auth")
    public ResponseEntity<UserDTO> auth(Principal principal) throws NotFoundException {
        return ResponseEntity.ok(modelMapper.map(
                service.getUserByName(principal.getName()), UserDTO.class));
    }
}
