package by.training.finalproject.model.dto;

import by.training.finalproject.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.Data;

@Data
public class CommentDTO {
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private Date date;
    
    private String user;
        
    private String text;

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user.getFirstName() + " " + user.getLastName();
    }
}
