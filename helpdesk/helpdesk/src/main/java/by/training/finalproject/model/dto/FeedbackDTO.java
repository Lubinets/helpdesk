package by.training.finalproject.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FeedbackDTO {
    private short rate;
    
    private String text;
    
    public FeedbackDTO(short rate, String text) {
        this.rate = rate;
        this.text = text;
    }
}
