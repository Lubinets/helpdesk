package by.training.finalproject.model;


public enum Urgency {
    LOW,
    MEDIUM,
    HIGH,
    CRITICAL
}
