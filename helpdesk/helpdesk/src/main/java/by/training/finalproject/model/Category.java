package by.training.finalproject.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Category")
@Data
public class Category implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;
    
    @Column(name = "name")
    private String name;
}
