package by.training.finalproject.repository.impl;

import by.training.finalproject.model.Role;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.*;
import java.util.List;
import java.util.Optional;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoH2 implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Optional<User> get(long id) {
        return Optional.ofNullable(
                sessionFactory.getCurrentSession()
                .get(User.class, id));
    }

    @Override
    public Optional getByName(String name) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from User where email = :login", User.class);
        query.setParameter("login", name);
        return Optional.ofNullable(query.list().get(0));
    }
    
    @Override
    public List<User> getByRole(Role role) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from User where role = :role", User.class);
        query.setParameter("role", role);
        return query.list();
    }
    
}
