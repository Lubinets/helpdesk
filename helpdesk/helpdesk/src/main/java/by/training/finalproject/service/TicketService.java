package by.training.finalproject.service;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Action;
import by.training.finalproject.model.Attachment;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.Urgency;
import by.training.finalproject.model.User;
import by.training.finalproject.model.dto.TicketCreateDTO;
import by.training.finalproject.repository.TicketDao;
import by.training.finalproject.repository.UserDao;
import by.training.finalproject.utility.Filter;
import by.training.finalproject.utility.Sorter;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import by.training.finalproject.repository.ActionDao;
import javax.mail.MessagingException;
import org.modelmapper.ModelMapper;

@Service
public class TicketService {

    private final TicketDao ticketDao;

    private final UserDao userDao;

    private final AttachmentService attachmentService;

    private final ActionDao stateChangeDao;

    private final ModelMapper mapper;
    
    private final HistoryService historyService;
    
    private final EmailService emailService;
    
    @Autowired
    public TicketService(TicketDao ticketDao, UserDao userDao,
            AttachmentService attachmentService, ActionDao stateChangeDao,
            ModelMapper mapper, HistoryService historyService,
            EmailService emailService) {
        this.ticketDao = ticketDao;
        this.userDao = userDao;
        this.attachmentService = attachmentService;
        this.stateChangeDao = stateChangeDao;
        this.mapper = mapper;
        this.historyService = historyService;
        this.emailService = emailService;
    }


    @Transactional
    public List<Ticket> getAllTickets(Map<String, String[]> paramMap) throws NotFoundException {
        List<Ticket> tickets;
        if (paramMap.isEmpty()) {
            tickets =  ticketDao.getAll();
            return isEmpty(tickets);
        } else {
            Optional<String[]> sortQuery = Optional.ofNullable(paramMap.get("sort"));
            Sorter sorter = null;
            if (sortQuery.isPresent()) {
                sorter = new Sorter(sortQuery.get()[0]);
            }
            Filter filter = new Filter(paramMap);
            setupTicketFilter(filter);
            tickets = ticketDao.getAll(filter, sorter);
            return isEmpty(tickets);
        }
    }

    @Transactional
    public List<Ticket> getByOwner(Map<String, String[]> paramMap, long id) throws NotFoundException {
        List<Ticket> tickets;
        if (paramMap.isEmpty()) {
            tickets = ticketDao.getByOwner(id);
            return isEmpty(tickets);
        } else {
            Optional<String[]> sortQuery = Optional.ofNullable(paramMap.get("sort"));
            Sorter sorter = null;
            if (sortQuery.isPresent()) {
                sorter = new Sorter(sortQuery.get()[0]);
            }
            Filter filter = new Filter(paramMap);
            setupTicketFilter(filter);
            tickets = ticketDao.getByOwner(id, filter, sorter);
            return isEmpty(tickets);
        }
    }

    @Transactional
    public List<Ticket> getManagerTickets(Map<String, String[]> paramMap, long id) throws NotFoundException {
        if (paramMap.isEmpty()) {
            return isEmpty(ticketDao.getManagerRelevantTickets(id));
        } else {
            Optional<String[]> sortQuery = Optional.ofNullable(paramMap.get("sort"));
            Optional<String[]> ticketsQuery = Optional.ofNullable(paramMap.get("tickets"));
            Sorter sorter = null;
            if (sortQuery.isPresent()) {
                sorter = new Sorter(sortQuery.get()[0]);
            }
            Filter filter = new Filter(paramMap);
            setupTicketFilter(filter);
            if (ticketsQuery.isPresent() && ticketsQuery.get()[0].equals("my")) {
                return isEmpty(ticketDao.getManagerTickets(id, filter, sorter));
            } else {
                return isEmpty(ticketDao.getManagerRelevantTickets(id, filter, sorter));
            }
        }
    }

    @Transactional
    public List<Ticket> getEngineerTickets(Map<String, String[]> paramMap, long id) throws NotFoundException {
        if (paramMap.isEmpty()) {
            return isEmpty(ticketDao.getEngineerTickets(id));
        } else {
            Optional<String[]> sortQuery = Optional.ofNullable(paramMap.get("sort"));
            Optional<String[]> ticketsQuery = Optional.ofNullable(paramMap.get("tickets"));
            Sorter sorter = null;
            if (sortQuery.isPresent()) {
                sorter = new Sorter(sortQuery.get()[0]);
            }
            Filter filter = new Filter(paramMap);
            setupTicketFilter(filter);
            if (ticketsQuery.isPresent() && ticketsQuery.get()[0].equals("my")) {
                return isEmpty(ticketDao.getByAssignee(id, filter, sorter));
            } else {
                return isEmpty(ticketDao.getEngineerTickets(id, filter, sorter));
            }
        }
    }

    @Transactional
    public Ticket getById(long id) throws NotFoundException {
        Optional<Ticket> ticketOpt = ticketDao.get(id);
        if (ticketOpt.isPresent()) {
            Ticket ticket = ticketOpt.get();
            Hibernate.initialize(ticket.getAttachments());
            return ticket;
        } else {
            throw new NotFoundException("Ticket not found");
        }
    }

    @Transactional
    public long createTicket(TicketCreateDTO ticketDTO,
            Principal principal,
            boolean isDraft,
            MultipartFile file)
            throws IOException, MessagingException {
        User owner = userDao.getByName(principal.getName()).get();
        Ticket ticket = new Ticket(ticketDTO, owner);
        if (Objects.nonNull(file)) {
            attachmentService.validateFile(file);
            ticket.addAttachment(attachmentService.multipartToAttachment(file));
        }
        if (isDraft) {
            ticket.setState(State.DRAFT);
        } else {
            ticket.setState(State.NEW);
            emailService.sendNewTicket(ticket);
        }
        long id = ticketDao.save(ticket);
        historyService.addCreateRecord(ticket);
        return id;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void changeTicketState(State state, long id, Principal principal)
            throws NotFoundException, NoPermissionException {
        Optional<Ticket> ticketOpt = ticketDao.get(id);
        Optional<User> userOpt = userDao.getByName(principal.getName());
        if (!ticketOpt.isPresent() || !userOpt.isPresent()) {
            throw new NotFoundException();
        }
        Ticket ticket = ticketOpt.get();
        User user = userOpt.get();
        
        Optional<Action> allowedAction = stateChangeDao.isExist(ticket, state, user);
        if (allowedAction.isPresent()) {
            assignUser(state, ticket, user);
            State oldState = ticket.getState();
            ticket.setState(state);
            ticketDao.update(ticket);
            historyService.addStatusChangeRecord(ticket, oldState, user);
            emailService.sendEmail(allowedAction.get(), ticket);
        } else {
            throw new NoPermissionException("Action is now allowed");
        }
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void updateTicket(long id,
            TicketCreateDTO dto,
            Principal principal,
            MultipartFile file,
            boolean isDraft)
            throws NotFoundException, NoPermissionException, IOException {
        Optional<Ticket> ticketOpt = ticketDao.get(id);
        Optional<User> userOpt = userDao.getByName(principal.getName());
        if (!ticketOpt.isPresent() || !userOpt.isPresent()) {
            throw new NotFoundException();
        }
        Ticket ticket = ticketOpt.get();
        User user = userOpt.get();

        if (Objects.nonNull(file)) {
            attachmentService.validateFile(file);
            Attachment attachment = attachmentService.multipartToAttachment(file);
            ticket.addAttachment(attachment);
            historyService.addAttachmentAddedRecord(ticket, user, attachment);
        }

        if (!ticket.getTicketOwner().equals(user) || !ticket.getState().equals(State.DRAFT)) {
            throw new NoPermissionException("Only tickets with draft status can be edited");
        }

        mapper.map(dto, ticket);
        if (!isDraft) {
            ticket.setState(State.NEW);
            historyService.addStatusChangeRecord(ticket, State.DRAFT, user);
        }
        ticketDao.update(ticket);
        historyService.addEditRecord(ticket);
    }

    private void setupTicketFilter(Filter filter) {
        filter.setConstraint("id", Integer.class);
        filter.setConstraint("urgency", Urgency.class);
        filter.setConstraint("name", String.class);
        filter.setConstraint("state", State.class);
        filter.setConstraint("desiredResolutionTime", Date.class);
    }

    private void assignUser(State newState, Ticket ticket, User user) {
        if (newState.equals(State.APPROVED)) {
            ticket.setApprover(user);
        } else if (newState.equals(State.INPROGRESS)) {
            ticket.setTicketAssignee(user);
        }
    }
    
    private List<Ticket> isEmpty(List<Ticket> tickets) throws NotFoundException {
        if (!tickets.isEmpty()) {
            return tickets;
        } else {
            throw new NotFoundException();
        }
    }
}
