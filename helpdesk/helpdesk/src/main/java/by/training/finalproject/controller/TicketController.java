package by.training.finalproject.controller;

import by.training.finalproject.exception.NoPermissionException;
import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.State;
import by.training.finalproject.model.Ticket;
import by.training.finalproject.model.dto.TicketCreateDTO;
import by.training.finalproject.model.dto.TicketDTO;
import by.training.finalproject.model.dto.TicketOverviewDTO;
import by.training.finalproject.service.TicketService;
import java.io.IOException;
import java.lang.reflect.Type;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(path = "/tickets", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin
public class TicketController {

    private TicketService service;

    private ModelMapper modelMapper;

    @Autowired
    public TicketController(ModelMapper modelMapper, TicketService service) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @Secured({"ROLE_EMPLOYEE", "ROLE_MANAGER"})
    @PostMapping(consumes = "multipart/form-data")
    public ResponseEntity<String> createTicket(
            @RequestPart("ticket") TicketCreateDTO ticket,
            @RequestPart(name = "file", required = false) MultipartFile file,
            @RequestParam("draft") boolean isDraft,
            Principal principal,
            UriComponentsBuilder builder) throws IOException, MessagingException {
        long id = service.createTicket(ticket, principal, isDraft, file);
        UriComponents uriComponents
                = builder.path("/tickets/{id}").buildAndExpand(id);
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<TicketOverviewDTO> getById(@PathVariable(value = "id") long ticketId)
            throws NotFoundException {
        return ResponseEntity.ok(modelMapper.map(service.getById(ticketId), TicketOverviewDTO.class));
    }

    @Secured("ROLE_MANAGER")
    @GetMapping(path = "/managers/{id}")
    public ResponseEntity<List<Ticket>> getManagerTickets(
            @PathVariable(value = "id") long id,
            WebRequest webRequest) throws NotFoundException {
        Map<String, String[]> paramMap = webRequest.getParameterMap();
        Type listType = new TypeToken<List<TicketDTO>>() {
        }.getType();
        return ResponseEntity.ok(modelMapper.map(
                service.getManagerTickets(paramMap, id), listType));
    }

    @Secured("ROLE_EMPLOYEE")
    @GetMapping(path = "/employees/{id}")
    public ResponseEntity<List<Ticket>> getEmployeeTickets(
            @PathVariable(value = "id") long id,
            WebRequest webRequest) throws NotFoundException {
        Map<String, String[]> paramMap = webRequest.getParameterMap();
        Type listType = new TypeToken<List<TicketDTO>>() {
        }.getType();
        return ResponseEntity.ok(modelMapper.map(
                service.getByOwner(paramMap, id), listType));
    }

    @Secured("ROLE_ENGINEER")
    @GetMapping(path = "/engineers/{id}")
    public ResponseEntity<List<Ticket>> getEngineerTickets(
            @PathVariable(value = "id") long id,
            WebRequest webRequest) throws NotFoundException {
        Map<String, String[]> paramMap = webRequest.getParameterMap();
        Type listType = new TypeToken<List<TicketDTO>>() {
        }.getType();
        return ResponseEntity.ok(modelMapper.map(
                service.getEngineerTickets(paramMap, id), listType));
    }

    @PatchMapping(path = "/{id}")
    public ResponseEntity<?> changeTicketState(
            @PathVariable(value = "id") long id,
            @RequestParam(value = "state") State state,
            Principal principal) throws NotFoundException, NoPermissionException,
            MessagingException {
        service.changeTicketState(state, id, principal);
        return ResponseEntity.noContent().build();
    }

    @Secured({"ROLE_EMPLOYEE", "ROLE_MANAGER"})
    @PostMapping(path = "/{id}", consumes = "multipart/form-data")
    public ResponseEntity<?> editTicket(
            @RequestPart("ticket") TicketCreateDTO ticket,
            @RequestPart(name = "file", required = false) MultipartFile file,
            @PathVariable(value = "id") long id,
            @RequestParam(value = "draft") boolean isDraft,
            Principal principal)
            throws NotFoundException, NoPermissionException, IOException {
        service.updateTicket(id, ticket, principal, file, isDraft);
        return ResponseEntity.noContent().build();
    }
}
