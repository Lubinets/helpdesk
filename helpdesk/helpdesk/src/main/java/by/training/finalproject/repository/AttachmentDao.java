package by.training.finalproject.repository;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Attachment;
import java.util.Optional;


public interface AttachmentDao {
    /**
     * Return attachment by id.
     * @param id id of the attachment
     * @return Optional of attachment
     */
    public Optional<Attachment> getById(long id);
    
    /**
     * Removes attachment from the database.
     * @param id id of the attachment
     * @throws NotFoundException if no attachment found with such id
     */
    public void removeAttachment(long id) throws NotFoundException;
}
