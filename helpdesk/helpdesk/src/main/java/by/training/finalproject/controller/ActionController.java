package by.training.finalproject.controller;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.State;
import by.training.finalproject.service.ActionService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/actions", produces = MediaType.APPLICATION_JSON_VALUE)
public class ActionController {

    @Autowired
    private ActionService service;

    @GetMapping
    public ResponseEntity<?> getActionsList(
            @RequestParam State state,
            Principal principal) throws NotFoundException {
        return ResponseEntity.ok(service.getAllAllowed(state, principal.getName()));
    }
}
