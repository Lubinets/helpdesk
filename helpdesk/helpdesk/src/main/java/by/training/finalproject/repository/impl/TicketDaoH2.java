package by.training.finalproject.repository.impl;

import by.training.finalproject.model.Ticket;
import by.training.finalproject.repository.*;
import by.training.finalproject.utility.Filter;
import by.training.finalproject.utility.Sorter;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TicketDaoH2 implements TicketDao {
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public long save(Ticket ticket) {
        return (long) sessionFactory.getCurrentSession()
                .save(ticket);
    }

    @Override
    public Optional<Ticket> get(long id) {
        return Optional.ofNullable(
                sessionFactory.getCurrentSession()
                .get(Ticket.class, id));
    }

    @Override
    public void update(Ticket ticket) {
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Override
    public List<Ticket> getAll() {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Ticket", Ticket.class);
        return query.list();
    }
    
    @Override
    public List<Ticket> getAll(Filter filter, Sorter sorter) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        List<Predicate> predicates = filter.build(builder, root);
        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        if (sorter != null) {
            criteriaQuery.orderBy(sorter.build(builder, root));
        }
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }

    @Override
    public List<Ticket> getByOwner(long id) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Ticket where ticketOwner.id =:id order by urgency desc", Ticket.class);
        query.setParameter("id", id);
        return query.list();
    }
    
    @Override
    public List<Ticket> getByOwner(long id, Filter filter, Sorter sorter) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        List<Predicate> predicates = filter.build(builder, root);
        predicates.add(builder.equal(root.get("ticketOwner").get("id"), id));
        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        if (sorter != null) {
            criteriaQuery.orderBy(sorter.build(builder, root));
        }
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
    @Override
    public List<Ticket> getEngineerTickets(long id, Filter filter, Sorter sorter) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate firstPredicate = builder.and(builder.equal(root.get("ticketAssignee").get("id"), id),
                builder.or(
                builder.equal(root.get("state"), 5),
                builder.equal(root.get("state"), 6)));
        Predicate finalPredicate = builder.or(
                builder.equal(root.get("state"), 2),
                firstPredicate);
        List<Predicate> predicates = filter.build(builder, root);
        predicates.add(finalPredicate);
        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        if (sorter != null) {
            criteriaQuery.orderBy(sorter.build(builder, root));
        }
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
    @Override
    public List<Ticket> getEngineerTickets(long id) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate firstPredicate = builder.and(builder.equal(root.get("ticketAssignee").get("id"), id),
                builder.or(
                builder.equal(root.get("state"), 5),
                builder.equal(root.get("state"), 6)));
        Predicate finalPredicate = builder.or(
                builder.equal(root.get("state"), 2),
                firstPredicate);
        criteriaQuery.select(root).where(finalPredicate);
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
    @Override
    public List<Ticket> getManagerRelevantTickets(long id) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate firstPredicate = builder.and(builder.equal(root.get("approver").get("id"), id),
                builder.or(
                        builder.notEqual(root.get("state"), 1),
                        builder.notEqual(root.get("state"), 0)));
        Predicate finalPredicate = builder.or(firstPredicate,
                builder.equal(root.get("state"), 1),
                builder.equal(root.get("ticketOwner").get("id"), id));
        criteriaQuery.select(root).where(finalPredicate);
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
    @Override
    public List<Ticket> getManagerRelevantTickets(long id, Filter filter, Sorter sorter) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate firstPredicate = builder.and(builder.equal(root.get("approver").get("id"), id),
                builder.or(
                        builder.notEqual(root.get("state"), 1),
                        builder.notEqual(root.get("state"), 0)));
        Predicate finalPredicate = builder.or(firstPredicate,
                builder.equal(root.get("state"), 1),
                builder.equal(root.get("ticketOwner").get("id"), id));
        List<Predicate> predicates = filter.build(builder, root);
        predicates.add(finalPredicate);
        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        if (sorter != null) {
            criteriaQuery.orderBy(sorter.build(builder, root));
        }
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
    @Override
    public List<Ticket> getByAssignee(long id) {
        Query query = sessionFactory.getCurrentSession()
                .createQuery("from Ticket where ticketAssignee.id =:id order by urgency desc", Ticket.class);
        query.setParameter("id", id);
        return query.list();
    }
    
    @Override
    public List<Ticket> getByAssignee(long id, Filter filter, Sorter sorter) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate predicate = builder.equal(root.get("ticketAssignee").get("id"), id);
        List<Predicate> predicates = filter.build(builder, root);
        predicates.add(predicate);
        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        if (sorter != null) {
            criteriaQuery.orderBy(sorter.build(builder, root));
        }
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
            
    @Override
    public List<Ticket> getManagerTickets(long id) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate firstPredicate = builder.equal(root.get("approver").get("id"), id);
        Predicate finalPredicate = builder.or(firstPredicate,
                builder.equal(root.get("ticketOwner").get("id"), id));
        criteriaQuery.select(root).where(finalPredicate);
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
    @Override
    public List<Ticket> getManagerTickets(long id, Filter filter, Sorter sorter) {
        CriteriaBuilder builder = sessionFactory
                .getCurrentSession()
                .getCriteriaBuilder();
        CriteriaQuery<Ticket> criteriaQuery = builder.createQuery(Ticket.class);
        Root<Ticket> root = criteriaQuery.from(Ticket.class);
        Predicate firstPredicate = builder.equal(root.get("approver").get("id"), id);
        Predicate finalPredicate = builder.or(firstPredicate,
                builder.equal(root.get("ticketOwner").get("id"), id));
        List<Predicate> predicates = filter.build(builder, root);
        predicates.add(finalPredicate);
        criteriaQuery.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        if (sorter != null) {
            criteriaQuery.orderBy(sorter.build(builder, root));
        }
        
        Query<Ticket> query = sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery);
        return query.getResultList();
    }
    
   
}
