package by.training.finalproject.model.dto;

import by.training.finalproject.model.Role;
import lombok.Data;

@Data
public class UserDTO {
    private long id;
    
    private String firstName;
    
    private String lastName;
    
    private Role role;
}
