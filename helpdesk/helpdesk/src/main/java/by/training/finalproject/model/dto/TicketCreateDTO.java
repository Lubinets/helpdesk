package by.training.finalproject.model.dto;

import by.training.finalproject.model.Category;
import by.training.finalproject.model.Urgency;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.Data;

@Data
public class TicketCreateDTO {
    private Category category;
    
    private String name;
    
    private String description;
    
    private Urgency urgency;
    
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date desiredResolutionDate;
        
    private String comment;
    
    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = new Category();
        this.category.setId(Long.valueOf(category));
    }
}
