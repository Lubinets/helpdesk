package by.training.finalproject.service;

import by.training.finalproject.exception.NotFoundException;
import by.training.finalproject.model.Action;
import by.training.finalproject.model.State;
import by.training.finalproject.model.User;
import by.training.finalproject.repository.ActionDao;
import by.training.finalproject.repository.UserDao;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ActionService {
    @Autowired
    private ActionDao stateChangeDao;
    
    @Autowired
    private UserDao userDao;
    
    @Transactional
    public List<Action> getAllAllowed(State state, String username) throws NotFoundException {
        Optional<User> userOpt = userDao.getByName(username);
        if(!userOpt.isPresent()) {
            throw new NotFoundException();
        }
        List<Action> actions = stateChangeDao.getAllAllowed(state, userOpt.get().getRole());
        if(!actions.isEmpty()) {
            return actions;
        } else {
            throw new NotFoundException("No available actions");
        }
    }
}
