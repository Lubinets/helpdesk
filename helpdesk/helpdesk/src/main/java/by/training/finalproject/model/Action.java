package by.training.finalproject.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Action")
@Data
public class Action implements Serializable {
    @Id
    @Column(name = "id")
    private long id;
    
    @Column(name = "name")
    private String name;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "role_id")
    private Role role;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "old_state")
    private State oldState;
    
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "new_state")
    private State newState;
    
    @Column(name = "mail_template")
    private String mailTemplate;
}
