package by.training.finalproject.config.security;

import by.training.finalproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class TicketSecurity {
    @Autowired
    private UserService service;
    
    public boolean hasAccess(Authentication authentication, int id) {
        try {
            User user = (User) authentication.getPrincipal();
            if (id == service.getUserByName(user.getUsername()).getId()) {
                return true;
            } else { 
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }
}
