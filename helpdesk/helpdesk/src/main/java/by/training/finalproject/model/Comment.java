package by.training.finalproject.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;

@Entity
@Table(name = "Comment")
@Data
public class Comment implements Serializable {
    private final static String regexp = "^[a-zA-Z0-9~.(),:;<>@\\[\\]!#$%&*+-\\/=?^_{|}]+$";
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    
    @Column(name = "text")
    @Size(max = 100)
    @Pattern(regexp = regexp)
    private String text;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date")
    private Date date;
    
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;
}
